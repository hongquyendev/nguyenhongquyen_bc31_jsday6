// bài 1

document.getElementById("btnbai1").addEventListener("click", function () {
  var sum = 0;
  var i = 0;
  while (sum <= 10000) {
    i++;
    sum = sum + i;
  }

  document.getElementById(
    "ketquabai1"
  ).innerHTML = `<div>Số nguyên dương nhỏ nhất cần tìm là ${i}</div>`;
});

// Bài 2

document.getElementById("btnbai2").addEventListener("click", function () {
  var x = document.getElementById("x_number").value * 1;
  var n = document.getElementById("n_number").value * 1;
  var sum = 0;
  for (i = 1; i <= n; i++) {
    sum += x ** i;
  }
  document.getElementById("ketquabai2").innerHTML = `<div>
${sum}
</div>`;
});

// Bài 3

document.getElementById("btnbai3").addEventListener("click", function () {
  var n = document.getElementById("value_bai3").value * 1;
  var p = 1;
  for (i = 1; i <= n; i++) {
    p *= i;
  }
  document.getElementById("ketquabai3").innerHTML = `<div>Kết quả: ${p}</div>`;
});

// Bài 4

document.getElementById("btnbai4").addEventListener("click", function () {
  var laymaubg = function (i) {
    if (i % 2 == 0) {
      return `<div class="bg-danger">div chẵn ${i}</div>`;
    } else {
      return `<div class="bg-primary">div lẻ ${i}</div>`;
    }
  };

  var content = "";
  for (var i = 1; i <= 10; i++) {
    content += laymaubg(i);
  }

  document.getElementById("ketquabai4").innerHTML = content;
});
